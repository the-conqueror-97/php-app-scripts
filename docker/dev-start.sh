#!/bin/sh

echo "Starting developpement in docker in background"
docker compose --env-file .docker.env up --build -d

echo "If you want to stop server you can simple run 'docker-compose down'"

echo "Opening project site"
python3 -m webbrowser http://localhost:3000

echo "Opening phpmyadmin"
python3 -m webbrowser http://localhost:3001

echo "Opening MailDev"
python3 -m webbrowser http://localhost:3002

echo "Entering in the app automatically"
docker exec -it $(basename $PWD)-php bash

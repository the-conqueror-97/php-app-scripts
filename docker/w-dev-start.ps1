Write-Host "Running on Windows"

# Check if Python 3 is installed
if (!(Get-Command python3 -ErrorAction SilentlyContinue)) {
    Write-Host "Python 3 is not installed, installing now..."
    python
}
else {
    Write-Host "Python 3 is already installed."
}

# Check if Docker is installed
if (!(Get-Command docker -ErrorAction SilentlyContinue)) {
    Write-Host "Docker is not installed, installing now..."
    # Add Windows-specific Docker installation steps here
    Invoke-WebRequest "https://desktop.docker.com/win/stable/Docker%20Desktop%20Installer.exe" -OutFile "docker-installer.exe"
    Start-Process -Wait -FilePath ".\docker-installer.exe" -ArgumentList "/silent"
    Remove-Item -Force "docker-installer.exe"
    Write-Host "Docker has been installed successfully."
}
else {
    Write-Host "Docker is already installed."
}

# Check if Docker Compose is installed
if (!(Get-Command docker-compose -ErrorAction SilentlyContinue)) {
    Write-Host "Docker Compose is not installed, installing now..."
    # Add Windows-specific Docker Compose installation steps here
    Invoke-WebRequest "https://github.com/docker/compose/releases/latest/download/docker-compose-Windows-x86_64.exe" -OutFile "docker-compose.exe"
    Move-Item -Path ".\docker-compose.exe" -Destination "$Env:ProgramFiles\Docker\"
    Write-Host "Docker Compose has been installed successfully."
}
else {
    Write-Host "Docker Compose is already installed."
}

docker-compose --env-file .docker.env up --build -d

Write-Host "Opening project site"
python -m webbrowser "http://localhost:3000"

Write-Host "Opening phpmyadmin"
python -m webbrowser "http://localhost:3001"

Write-Host "Opening MailDev"
python -m webbrowser "http://localhost:3002"

docker exec -it $((Get-Item -Path ".\").Name)-php bash

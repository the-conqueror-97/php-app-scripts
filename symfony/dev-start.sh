#!/bin/sh
echo "Starting symfony proxy"
symfony proxy:start
echo "Starting symfony server"
symfony server:start --port 3000 -d

echo "Opening into browser"
symfony open:local
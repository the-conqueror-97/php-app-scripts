#!/bin/sh

echo "This will reset the full project"


bash scripts/doctrine/drop-db.sh APP_ENV=$APP_ENV
bash scripts/doctrine/create-db.sh APP_ENV=$APP_ENV
bash scripts/doctrine/migrate-db.sh APP_ENV=$APP_ENV
bash scripts/doctrine/fixtures.sh APP_ENV=$APP_ENV